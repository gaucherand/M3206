#!/bin/bash

if [ "$(dpkg -s ssh)" != false ]; then
	echo "ssh est bien installé"
else
	"Je vais installer ssh..."
	apt-get install ssh
fi

if [ "/etc/init.d/ssh start" = true ]; then
	echo "ssh est bien lancé !"
else
	echo "Je vais procéder au lancement de ssh..."
	/etc/init.d/ssh start
fi

exit 0

