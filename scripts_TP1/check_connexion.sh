#!/bin/bash

wget -q --tries=3 --timeout=5 http://google.com #tries: permet de ping et timeout: arrete l'éxectution de la commande apres le temps indiqué (ici 5s)

if [[ $? -eq 0 ]]; then
	echo "En ligne !"
else
	echo "Pas d'internet !"
fi

exit 0
