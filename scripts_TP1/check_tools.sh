#!/bin/bash

if [ "$(dpkg -s git)" != "false" ]; then
	echo "git est bien installé"
else
	echo "git n'est pas installé, je vais procéder à l'instalation..."
	apt-get install git
fi

if [ "$(dpkg -s tmux)" != "false" ]; then
	echo "tmux est bien installé"
else
	echo "tmux n'est pas installé, je vais procéder à l'instalation..."
	apt-get install tmux
fi

if [ "$(dpkg -s vim)" != "false" ]; then
	echo "vim est bien installé"
else
	echo "vim n'est pas installé, je vais procéder à l'instalation..."
	apt-get install vim
fi

if [ "$(dpkg -s htop)" != "false" ]; then
	echo "htop est bien installé"
else
	echo "htop n'est pas installé, je vais procéder à l'instalation..."
	apt-get install htop
fi

exit 0
