git push : envoi un fichier vers mon interface

git pull : récupère un fichier depuis mon interface vers mon terminal

git add : permet d'ajouter un fichier dans le dépôt actuel

git commit : nomme les actions , permet de se retrouver

VI :
	- V (visual), permet de selectionner plusieurs ligne à copier coller avec y
	- yy, copie toute la ligne
	- p, colle

J'ai créé quelques alias...
gita = git add
gitcm = git commit -m
gitp = git push
