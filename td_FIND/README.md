```bash
find /tmp -name *.txt -ok rm {} \;
```
Cette commande demande la suppression un à un des fichiers ayant l’extension « .txt » dans le dossier tmp.

`-ok` permet de demander confirmation avant chaque action.

La commande `cpio` permet de faire des sauvegardes.
```bash
find . -type f -exec cp {} . \;
```
Cette commande copie récursivement tous les fichiers à partir du répertoire courant et les mets dans le dossier courant

```bash
head -n 1 *.txt
```
La commande `head` permet d'afficher les n premières lignes de texte d'un fichier.
/!\ PROBLEME SED + AWK, AFFICHE SEULEMENT 1 LIGNE DE UN SEUL DES 2 FICHIERS 1(test2) et 2 (test)

```bash
awk '{ print NF ":" $2 } ' test.txt
```
`$2` va afficher seulement le deuxième champ des lignes du fichier.
`$1` et `$3` : sont les premiers et derniers champs des lignes du fichier.
`$0` : affiche tout les champs des lignes.

```bash
awk 'END { print NF ":" $NF } ' test.txt
```
`$NF` affiche le dernier champ des lignes.
`END` permet de se positionner sur la dernière ligne du fichier.