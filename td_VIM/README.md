### ***Exercice 1***

Commande utilisée :

`Ctrl +d` pour descendre d'1/2 page

### ***Exercice 2***

Commande utilisée :

```bash 
:g/Sed/s//XXX/g
```
    
Permet de rechercher dans tous le fichier le mot **SED** et les remplace par **XXX**

### ***Exercice 3***

Commandes utilisées :

	1. v (mode VISUAL)
	2. ctrl +j (2 fois, pour selectionner les 2 lignes)
	3. d (pour supprimer)

### ***Exercice 4***

J'ai eu besoin de 6 touches pour changer les 99 V en 100 i :

*9, 9, r, i, i, i*
