#!/bin/bash

cat my_history | awk 'BEGIN {FS=";"}{print $2}' | cut -f1 -d ' ' | sort | uniq -c | sort -nr | head -n $1
